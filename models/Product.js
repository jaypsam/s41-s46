const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name:{
		type: String,
		require: [true, "Product Name is required"]
	},
	description: {
		type: String,
		require: [true, "Description is required"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	order: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			}
		}
	]
});


module.exports = mongoose.model("Product", productSchema);