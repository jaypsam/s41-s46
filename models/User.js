const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	orders:[{
        productId: {
            type: String,
            required: [true, "Product Id is required"]
        },
        productName: {
            type: String,
            required: [false, "Product Name is required"]
        },
        quantity: {
            type:Number,
            required: [false, "Quantity is required"]
        },
        totalAmount: {
            type: Number,
            required: [false, "Total amount required"]
        },
        purchasedOn: {
                type: Date,
                default: new Date(),

        },
        status:{
            	type: String,
            	default: "Ordered"
            }
        }]

})


module.exports = mongoose.model("User", userSchema);
